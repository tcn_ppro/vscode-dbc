# vscode-dbc README

DB/C Language support for Visual Studio Code
[dbcsoftware.com](http://dbcsoftware.com/)

## Features

- Syntax highlighting
- Specifies file encoding settings
- Folding for Routines

## Known Issues

Toggle Comments assume indentation. Which DB/C does not support. No known workaround for now.

-----------------------------------------------------------------------------------------------------------
## Release Notes

### 1.0.0

Initial release

